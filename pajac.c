/*
  Le paja Compiler
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#include "compiler.h"

bool eq(char * s, const char * n){
  const char * e = s;
  if (e == n){
    return true;
  }
  else {
    return false;
  }
}
void compile(char* s, char* a){
  if(eq(s, S_STDOUT)){
    printf("%s",a);
  }
}

int open_file(char* a){
  FILE * source;
  char ch;
  char* instr = NULL;
  char* argument = NULL;
  bool rargs = false;
  source = fopen(a, "r");
  if(source == NULL){
        printf("Unable to open file.\n");
	return 1;
    }
  
    do {
        ch = fgetc(source);
	if(ch != '\n' && ch != ' ' && ch != '\t'){
	  // getting le source
	  if(ch != END_I){
	    if(ch == S_ARGS){
	      rargs = true;
	      continue;
	    }
	    if(!rargs)
	      strncat(instr, &ch, 1);
	    else
	      strncat(argument, &ch, 1);
	  }
	  else {
	    compile(instr, argument);
	  }
	}
    } while(ch != EOF);

    fclose(source);
    return 0;
}

int main(int argc, char** argv){
  int result = 1;
  int c;
  char *source = NULL;
  while((c = getopt (argc, argv, "s:h")) != -1){
    switch(c){
    case 's':
      source = optarg;
      break;
    case 'h':
      fprintf(stderr, "le paja compiler.\n\nUSAGE:\n\t-s [SOURCE]\n\t-h: help\n");
      break;
    }
  }
  result = open_file(source);
  printf("\n\nexitcode: %d\n", result);
  return result;
}
